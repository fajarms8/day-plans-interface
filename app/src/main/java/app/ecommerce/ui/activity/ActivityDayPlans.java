package app.ecommerce.ui.activity;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import app.ecommerce.ui.R;
import app.ecommerce.ui.adapter.AdapterDayPlans;
import app.ecommerce.ui.adapter.AdapterHighlight;
import app.ecommerce.ui.data.DataDayPlans;
import app.ecommerce.ui.data.DataHighlight;
import app.ecommerce.ui.model.ModelDayPlans;
import app.ecommerce.ui.model.ModelHighlight;
import app.ecommerce.ui.utils.Tools;

public class ActivityDayPlans extends AppCompatActivity {

    RecyclerView recycler_view;
    RecyclerView recycler_highlight;
    AdapterDayPlans adapter;
    AdapterHighlight adapterHighlight;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_day_plan);

        initToolbar();
        initComponent();
    }

    private void initToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
        //change menu button color
        Tools.changeNavigationIconColor(toolbar, getResources().getColor(R.color.colorWhite));
        //change overflow menu button color
        Tools.changeOverflowMenuIconColor(toolbar, getResources().getColor(R.color.colorWhite));
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Day Plans");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Tools.setSystemBarColor(this, R.color.grey_5);
        Tools.setSystemBarLight(this);
    }

    private void initComponent() {
        // define recycler view for day plans list
        recycler_view = findViewById(R.id.recycler_view);
        recycler_view.setLayoutManager(new LinearLayoutManager(this));
        recycler_view.setHasFixedSize(true);
        recycler_view.setNestedScrollingEnabled(false);

        final List<ModelDayPlans> items = DataDayPlans.getPlan(this);
        //items.addAll(DataDayPlans.getPlan(this));

        //set data and list adapter
        adapter = new AdapterDayPlans(this, items);
        recycler_view.setAdapter(adapter);

        // define recycler view for highlight
        recycler_highlight = findViewById(R.id.recycler_highlight);
        RecyclerView.LayoutManager mLayoutManager1 = new GridLayoutManager(this, 3);
        recycler_highlight.setLayoutManager(mLayoutManager1);
        recycler_highlight.setItemAnimator(new DefaultItemAnimator());
        recycler_highlight.setHasFixedSize(true);
        recycler_highlight.setNestedScrollingEnabled(false);

        final List<ModelHighlight> itemsHighlight = DataHighlight.getHighlight(this);
        //items.addAll(DataDayPlans.getPlan(this));

        //set data and list adapter
        adapterHighlight = new AdapterHighlight(this, itemsHighlight);
        recycler_highlight.setAdapter(adapterHighlight);

        // on item list clicked
        /*adapter.setOnItemClickListener(new AdapterDayPlans.OnItemClickListener() {
            @Override
            public void onItemClick(View view, ModelDayPlans obj, int position) {
                Toast.makeText(getApplicationContext(), "Item " + obj.title + " clicked", Toast.LENGTH_SHORT).show();
            }
        });*/
    }
}
