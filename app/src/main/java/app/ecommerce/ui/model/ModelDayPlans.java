package app.ecommerce.ui.model;

import android.graphics.drawable.Drawable;

public class ModelDayPlans {

    public int image;
    public Drawable imageMap;
    public String title;
    public String sights;
    public String description;

    public ModelDayPlans() {
    }

}
