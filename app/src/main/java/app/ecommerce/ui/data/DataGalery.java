package app.ecommerce.ui.data;

import android.content.Context;
import android.content.res.TypedArray;

import java.util.ArrayList;
import java.util.List;

import app.ecommerce.ui.R;
import app.ecommerce.ui.model.ModelGalery;

@SuppressWarnings("ResourceType")
public class DataGalery {

    /**
     * Generate dummy data shopping product
     *
     * @param ctx android context
     * @return list of object
     */
    public static List<ModelGalery> getGalery(Context ctx) {
        List<ModelGalery> items = new ArrayList<>();
        TypedArray drw_arr = ctx.getResources().obtainTypedArray(R.array.galery_image);
        for (int i = 0; i < drw_arr.length(); i++) {
            ModelGalery obj = new ModelGalery();
            obj.image = drw_arr.getResourceId(i, -1);
            obj.imageDrw = ctx.getResources().getDrawable(obj.image);
            items.add(obj);
        }
        return items;
    }
}
