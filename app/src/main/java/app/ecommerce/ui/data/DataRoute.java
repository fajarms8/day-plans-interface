package app.ecommerce.ui.data;

import android.content.Context;
import android.content.res.TypedArray;

import java.util.ArrayList;
import java.util.List;

import app.ecommerce.ui.R;
import app.ecommerce.ui.model.ModelRoute;

@SuppressWarnings("ResourceType")
public class DataRoute {

    /**
     * Generate dummy data shopping product
     *
     * @param ctx android context
     * @return list of object
     */
    public static List<ModelRoute> getRoute(Context ctx) {
        List<ModelRoute> items = new ArrayList<>();
        TypedArray drw_arr = ctx.getResources().obtainTypedArray(R.array.route_duration);
        String duration_arr[] = ctx.getResources().getStringArray(R.array.route_duration);
        String place_arr[] = ctx.getResources().getStringArray(R.array.route_place);
        String open_arr[] = ctx.getResources().getStringArray(R.array.route_open);
        String time_arr[] = ctx.getResources().getStringArray(R.array.route_time);
        for (int i = 0; i < drw_arr.length(); i++) {
            ModelRoute obj = new ModelRoute();
            obj.duration = duration_arr[i];
            obj.place = place_arr[i];
            obj.open = open_arr[i];
            obj.time = time_arr[i];
            items.add(obj);
        }
        return items;
    }
}
