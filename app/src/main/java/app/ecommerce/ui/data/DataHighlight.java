package app.ecommerce.ui.data;

import android.content.Context;
import android.content.res.TypedArray;

import java.util.ArrayList;
import java.util.List;

import app.ecommerce.ui.R;
import app.ecommerce.ui.model.ModelHighlight;

@SuppressWarnings("ResourceType")
public class DataHighlight {

    /**
     * Generate dummy data shopping product
     *
     * @param ctx android context
     * @return list of object
     */
    public static List<ModelHighlight> getHighlight(Context ctx) {
        List<ModelHighlight> items = new ArrayList<>();
        TypedArray drw_arr = ctx.getResources().obtainTypedArray(R.array.highlight_image);
        for (int i = 0; i < drw_arr.length(); i++) {
            ModelHighlight obj = new ModelHighlight();
            obj.image = drw_arr.getResourceId(i, -1);
            obj.imageDrw = ctx.getResources().getDrawable(obj.image);
            items.add(obj);
        }
        return items;
    }
}
