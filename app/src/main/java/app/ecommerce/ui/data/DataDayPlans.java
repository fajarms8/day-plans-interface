package app.ecommerce.ui.data;

import android.content.Context;
import android.content.res.TypedArray;

import java.util.ArrayList;
import java.util.List;

import app.ecommerce.ui.R;
import app.ecommerce.ui.model.ModelDayPlans;

@SuppressWarnings("ResourceType")
public class DataDayPlans {

    /**
     * Generate dummy data shopping product
     *
     * @param ctx android context
     * @return list of object
     */
    public static List<ModelDayPlans> getPlan(Context ctx) {
        List<ModelDayPlans> items = new ArrayList<>();
        TypedArray drw_arr = ctx.getResources().obtainTypedArray(R.array.plan_image);
        String title_arr[] = ctx.getResources().getStringArray(R.array.plan_title);
        String sight_arr[] = ctx.getResources().getStringArray(R.array.plan_sight);
        String description_arr[] = ctx.getResources().getStringArray(R.array.plan_description);
        for (int i = 0; i < drw_arr.length(); i++) {
            ModelDayPlans obj = new ModelDayPlans();
            obj.image = drw_arr.getResourceId(i, -1);
            obj.title = title_arr[i];
            obj.sights = sight_arr[i];
            obj.description = description_arr[i];
            obj.imageMap = ctx.getResources().getDrawable(obj.image);
            items.add(obj);
        }
        return items;
    }
}
