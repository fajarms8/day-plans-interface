package app.ecommerce.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import app.ecommerce.ui.R;
import app.ecommerce.ui.data.DataGalery;
import app.ecommerce.ui.data.DataRoute;
import app.ecommerce.ui.model.ModelGalery;
import app.ecommerce.ui.model.ModelDayPlans;
import app.ecommerce.ui.model.ModelRoute;

public class AdapterDayPlans extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private AdapterGalery adapterGalery;
    private AdapterRoute adapterRoute;
    private List<ModelDayPlans> items;

    private Context ctx;
    private OnItemClickListener mOnItemClickListener;
    //public AdapterGaleryListener onClickListener;

    public interface OnItemClickListener {
        void onItemClick(View view, ModelDayPlans obj, int position);
    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mOnItemClickListener = mItemClickListener;
    }

    public AdapterDayPlans(Context context, List<ModelDayPlans> items) {
        this.items = items;
        ctx = context;
    }

    public class OriginalViewHolder extends RecyclerView.ViewHolder {
        public TextView txttitle;
        public TextView txtsight;
        public TextView txtDesc;

        public ImageView img_arrow;

        public RecyclerView recycler_galery;
        public RecyclerView recycler_route;
        //public ImageView image;
        //public View lyt_parent;

        public OriginalViewHolder(View v) {
            super(v);
            txttitle = v.findViewById(R.id.txttitle);
            txtsight = v.findViewById(R.id.txtsight);
            txtDesc = v.findViewById(R.id.txtDesc);

            img_arrow = v.findViewById(R.id.img_arrow);

            recycler_galery = v.findViewById(R.id.recycler_galery);
            recycler_route = v.findViewById(R.id.recycler_route);

            /*img_arrow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onClickListener.viewRoute(v, getAdapterPosition());
                }
            });*/

            //image = v.findViewById(R.id.image);
            //lyt_parent = v.findViewById(R.id.lyt_parent);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.place_item, parent, false);
        vh = new OriginalViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof OriginalViewHolder) {
            final OriginalViewHolder view = (OriginalViewHolder) holder;

            ModelDayPlans p = items.get(position);
            view.txttitle.setText(p.title);
            view.txtsight.setText(p.sights);
            view.txtDesc.setText(p.description);

            view.img_arrow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (view.recycler_route.getVisibility() == View.VISIBLE){
                        view.recycler_route.setVisibility(View.GONE);
                        view.img_arrow.setImageResource(R.drawable.ic_keyboard_arrow_down_black_24dp);
                    } else {
                        view.recycler_route.setVisibility(View.VISIBLE);
                        view.img_arrow.setImageResource(R.drawable.ic_keyboard_arrow_up_black_24dp);
                    }

                }
            });

            //Tools.displayImageOriginal(ctx, view.image, p.image);

            //recyclverview_galery
            RecyclerView.LayoutManager mLayoutManager1 = new LinearLayoutManager(ctx, LinearLayoutManager.HORIZONTAL, false);
            view.recycler_galery.setLayoutManager(mLayoutManager1);
            view.recycler_galery.setHasFixedSize(true);
            view.recycler_galery.setNestedScrollingEnabled(true);

            List<ModelGalery> itemgalery = DataGalery.getGalery(ctx);
            //itemgalery.addAll(DataGalery.getGalery(ctx));

            //set data and list adapter
            adapterGalery = new AdapterGalery(ctx, itemgalery);
            view.recycler_galery.setAdapter(adapterGalery);

            //recyclverview_route
            RecyclerView.LayoutManager mLayoutManager2 = new LinearLayoutManager(ctx, LinearLayoutManager.VERTICAL, false);
            view.recycler_route.setLayoutManager(mLayoutManager2);
            view.recycler_route.setHasFixedSize(true);
            view.recycler_route.setNestedScrollingEnabled(true);

            List<ModelRoute> itemroute = DataRoute.getRoute(ctx);

            //set data and list adapter
            adapterRoute = new AdapterRoute(ctx, itemroute);
            view.recycler_route.setAdapter(adapterRoute);

            /*view.lyt_parent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mOnItemClickListener != null) {
                        mOnItemClickListener.onItemClick(view, items.get(position), position);
                    }
                }
            });*/
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void showDetail(int position) {

    }

    public interface AdapterGaleryListener {

        void viewRoute(View v, int position);

    }

}