package app.ecommerce.ui.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import app.ecommerce.ui.R;
import app.ecommerce.ui.model.ModelRoute;

public class AdapterRoute extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<ModelRoute> items;

    private Context ctx;
    private OnItemClickListener mOnItemClickListener;

    public interface OnItemClickListener {
        void onItemClick(View view, ModelRoute obj, int position);
    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mOnItemClickListener = mItemClickListener;
    }

    public AdapterRoute(Context context, List<ModelRoute> items) {
        this.items = items;
        ctx = context;
    }

    public class OriginalViewHolder extends RecyclerView.ViewHolder {
        public LinearLayout lyt_parent;
        public TextView txt_number;
        public TextView txt_duration;
        public TextView txt_place;
        public TextView txt_open;
        public TextView txt_time;
        public ImageView img_walk;

        public OriginalViewHolder(View v) {
            super(v);
            lyt_parent = v.findViewById(R.id.lyt_parent);
            txt_number = v.findViewById(R.id.txt_number);
            txt_duration = v.findViewById(R.id.txt_duration);
            txt_place = v.findViewById(R.id.txt_place);
            txt_open = v.findViewById(R.id.txt_open);
            txt_time = v.findViewById(R.id.txt_time);
            img_walk = v.findViewById(R.id.img_walk);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.route_item, parent, false);
        vh = new OriginalViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof OriginalViewHolder) {
            OriginalViewHolder view = (OriginalViewHolder) holder;

            ModelRoute p = items.get(position);
            view.txt_number.setText(String.valueOf(position+1));
            view.txt_duration.setText(p.duration);
            view.txt_place.setText(p.place);
            view.txt_open.setText(p.open);
            view.txt_time.setText(p.time);

            if (position % 2 == 0){
                view.lyt_parent.setBackgroundColor(Color.parseColor("#fafafb"));
            }

            if (p.duration.matches("")){
                view.txt_duration.setVisibility(View.GONE);
                view.img_walk.setVisibility(View.GONE);
            }

            if (p.open.matches("")){
                view.txt_open.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

}