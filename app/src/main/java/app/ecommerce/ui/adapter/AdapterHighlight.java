package app.ecommerce.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import app.ecommerce.ui.R;
import app.ecommerce.ui.model.ModelHighlight;
import app.ecommerce.ui.utils.Tools;

public class AdapterHighlight extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<ModelHighlight> items;

    private Context ctx;
    private OnItemClickListener mOnItemClickListener;

    public interface OnItemClickListener {
        void onItemClick(View view, ModelHighlight obj, int position);
    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mOnItemClickListener = mItemClickListener;
    }

    public AdapterHighlight(Context context, List<ModelHighlight> items) {
        this.items = items;
        ctx = context;
    }

    public class OriginalViewHolder extends RecyclerView.ViewHolder {
        public ImageView img_place;

        public OriginalViewHolder(View v) {
            super(v);
            img_place = v.findViewById(R.id.img_place);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.highlight_item, parent, false);
        vh = new OriginalViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof OriginalViewHolder) {
            OriginalViewHolder view = (OriginalViewHolder) holder;

            ModelHighlight p = items.get(position);
            Tools.displayImageOriginal(ctx, view.img_place, p.image);
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

}